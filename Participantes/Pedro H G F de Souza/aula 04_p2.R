# Aula 04 - pt 2 - manipulação avançada de dados

# Carrega base
data("iris")

# dplyr
install.packages("psych")
library(dplyr)
library(knitr)
df<-iris %>%
      dplyr::select(-c(3,4))  %>%  # seleciona todas as colunas, menos a 3a e a 4a
      dplyr::group_by(Species) %>%   # analise a seguir agrupada por especie
      dplyr::mutate(Sepal.Ratio = Sepal.Length/Sepal.Width,
                    lnSepal.Length = log(Sepal.Length))%>%
      dplyr::filter(Sepal.Ratio > 1.85) %>%
      dplyr::summarise(n.long.Sepal = n(),
                      skewness = psych::skew(Sepal.Length)) %>%
      knitr::kable()

